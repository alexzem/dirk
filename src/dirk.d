module dirk;

import std.range;
import std.algorithm;
import std.stdio;
import std.path;
import std.json;
import std.file;
import std.system;
import std.string;
import std.process;
import std.getopt;

const enum DIRK_VERSION = "1.0.0";

version(Posix)
	const enum OBJECT_FILE = ".o";
version(Windows)
	const enum OBJECT_FILE = ".obj";

struct Project {
	string		name;
	string		type;
	string		sourcePath;
	string[]	includePaths;
	string[]	libPaths;
	string[]	libs;
	string[]	packages;
	string[]	versions;
	string[]	dflags;

	this(JSONValue val) {
		assert(val.type == JSON_TYPE.OBJECT);
		JSONValue[string] elem = val.object;
		assert("name" in elem);
		assert("type" in elem);
		assert("source" in elem);
		name = elem["name"].str;
		type = elem["type"].str;
		sourcePath = elem["source"].str;
		if("include" in elem) includePaths = elem["include"].array.map!(a => a.str).array;
		if("libPaths" in elem) libPaths = elem["libPaths"].array.map!(a => a.str).array;
		if("libs" in elem) libs = elem["libs"].array.map!(a => a.str).array;
		if("pkgs" in elem) packages = elem["pkgs"].array.map!(a => a.str).array;
		if("versions" in elem) versions = elem["versions"].array.map!(a => a.str).array;
		if("dflags" in elem) dflags = elem["dflags"].array.map!(a => a.str).array;
	}
}

enum Config {
	Release,
	Debug,
	Test
}

enum Arch {
	X86,
	X64
}

string[] findSourceFiles(string dir) {
	assert(exists(dir));
	string[] entries;
	foreach(string entry; dirEntries(dir, SpanMode.depth)) {
		if(extension(entry) == ".d")
			entries ~= entry.idup;
	}
	return entries;
}

string findLib(string[] libPaths, string lib) {
	foreach(libPath; libPaths) {
		foreach(string entry; dirEntries(libPath, SpanMode.shallow)) {
			if(globMatch(entry,lib))
				return entry;
		}
	}
	return null;
}

bool compileProgram(Project proj, Config conf, Arch arch) {
	auto srcFiles = findSourceFiles(proj.sourcePath);
	string includePaths = "-I%s".format(proj.sourcePath);
	includePaths ~= " " ~ std.array.join(proj.includePaths.map!(a => "-I%s".format(a)),"");
	string libs = "";
	foreach(string libFile; proj.libs.map!(a => proj.libPaths.findLib(a))) {
		libs ~= " %s".format(libFile);
	}
	string config;
	switch(conf) {
	case Config.Release:
		config = "-release -O";
		break;
	case Config.Debug:
		config = "-debug -gc";
		break;
	case Config.Test:
		config = "-unittest -debug -gc";
		break;
	default:
		config = "-release -O";
		break;
	}
	string archstr;
	switch(arch) {
	case Arch.X64:
		archstr = "-m64";
		break;
	case Arch.X86:
	default:
		archstr = "";
		break;
	}
	string cmd = "dmd %s -odbin -ofbin/%s %s %s %s %s".format(config, proj.name, archstr, includePaths, std.array.join(srcFiles, " "), libs);
	int code = system(cmd);
	return (code == 0);
}

bool compileLibrary(Project proj, Config conf, Arch arch) {
	auto srcFiles = findSourceFiles(proj.sourcePath);
	string includePaths = "-I%s".format(proj.sourcePath);
	includePaths ~= " " ~ std.array.join(proj.includePaths.map!(a => "-I%s".format(a)),"");
	string libs = "";
	foreach(string libFile; proj.libs.map!(a => proj.libPaths.findLib(a))) {
		libs ~= " %s".format(libFile);
	}
	string config;
	switch(conf) {
	case Config.Release:
		config = "-release -O";
		break;
	case Config.Debug:
		config = "-debug -gc";
		break;
	case Config.Test:
		config = "-unittest -debug -gc";
		break;
	default:
		config = "-release -O";
		break;
	}
	string archstr;
	switch(arch) {
	case Arch.X64:
		archstr = "-m64";
		break;
	case Arch.X86:
	default:
		archstr = "";
		break;
	}
	string cmd = "dmd %s -Hdimport -H -odlib -of%s %s %s -lib %s %s".format(config, proj.name, archstr, includePaths, std.array.join(srcFiles, " "), libs);
	int code = system(cmd);
	return (code == 0);
}

bool compile(string[] args) {
	JSONValue val = parseJSON(cast(string)std.file.read("dirk.json"));
	auto proj = Project(val);
	string config = "Release";
	string architecture = "32";
	string[] pkgs = [];
	getopt(
		args,
		"config", &config,
		"arch", &architecture,
		"pkg", &pkgs
	);
	Config conf;
	switch(config) {
	default:
	case "Release":
		conf = Config.Release;
		break;
	case "Debug":
		conf = Config.Debug;
		break;
	case "Test":
		conf = Config.Test;
		break;
	}
	Arch arch;
	switch(architecture) {
	case "64":
		arch = Arch.X64;
		break;
	case "32":
	default:
		arch = Arch.X86;
		break;
	}
	switch(proj.type) {
	case "program":
		return proj.compileProgram(conf, arch);
	case "library":
		return proj.compileLibrary(conf, arch);
	default:
		return false;
	}
}

void main(string[] args) {
	assignChar = ':';
	bool pass;
	switch(args[0]) {
	case "build":
	default:
		pass = compile(args);
		break;
	}
	if(pass) {
		foreach(string file; dirEntries(".",SpanMode.shallow)) {
			if(extension(file) == OBJECT_FILE)
				std.file.remove(file);
		}
	}
}